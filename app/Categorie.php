<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Recipe;

class Categorie extends Model
{
    public function recipes()
    {
    	$this->belongsToMany(Recipe::class);
    }
}
