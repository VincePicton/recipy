<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Recipe;
use App\User;

class Like extends Model
{
    public function user()
    {
    	$this->belongsTo(User::class);
    }

    public function recipe()
    {
    	$this->belongsTo(Recipe::class);
    }
}
