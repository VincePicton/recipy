<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Recipe;

class Instruction extends Model
{
	public $timestamps = false;
	protected $fillable = [
		'recipe_id',
		'step_number',
		'instruction'
	];

    public function recipe()
    {
    	$this->belongsTo(Recipe::class);
    }
    
}
