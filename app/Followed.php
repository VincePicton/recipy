<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Followed extends Model
{
    public function user()
    {
    	$this->belongsTo(User::class);
    }
}
