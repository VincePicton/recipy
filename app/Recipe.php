<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

use App\Instruction;
use App\Categorie;
use App\Like;

class Recipe extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'description',
		'user_id',
		'serves',
		'img_link',
		'ingredients',
	];

	public function user()
	{
		return $this->belongsTo(User::class); //this tells laravel that recipe has a user which it belongs to, and somehow looks up what that team is in the database
	}

	public function instructions()
	{
		return $this->hasMany(Instruction::class);
	}

	public function categories()
	{
		return $this->belongsToMany(Categorie::class);
	}

	public function likes()
	{
		return $this->hasMany(Like::class);
	}
}
