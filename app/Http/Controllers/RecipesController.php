<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Delete;

//return dd($request->all());

class RecipesController extends Controller
{
    function index(Request $request)
    {
        $user_id = Auth::id();
        //orderBy('<field name>', '<direction>') - USEFUL FUNCTION
        $usersRecipes = Recipe::where('user_id', $user_id)
        ->latest() //should order by most recent
        ->paginate(20);
        return view('mainpage', ['usersRecipes' => $usersRecipes]);
    }

    function test($instructions)
    {
        return $instructions;
    }
       
    function show(Request $request, Recipe $recipe)
    {
        return view('RecipeDetails', ['recipe' => $recipe]);
        //return view('RecipeDetails', compact('id'));
    }

    function destroy(Request $request, Recipe $recipe)
    {
        $recipe->delete(); //delete is an inbuild and not visible method
        return redirect('/');
    }

    function edit($id)
    {
        $recipe = Recipe::find($id);
        return view('edit', ['recipe' => $recipe]);
    }

    function create(Request $request)
    {
        return view('CreateForm');
    }

    function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'serves' => 'required',
            'description' => 'required',
            'ingredients' => 'required',
            'step1' => 'required',
            'step2' => 'nullable',
            'step3' => 'nullable',
            'step4' => 'nullable',
            'step5' => 'nullable',
            'step6' => 'nullable',
            'step7' => 'nullable',
            'step8' => 'nullable',
            'step9' => 'nullable',
            'step10' => 'nullable',
            'image' => 'nullable'
            // USE THIS https://laravel.com/docs/5.8/validation
        ]);

        $recipe = Recipe::find($id);

        $file = $request->file('image');
        if($file)
        {
            $extension = $file->getClientOriginalExtension();
            Storage::disk('public')->put($file->getFilename().".".$extension, File::get($file));
            $image_name = $file->getFilename().".".$extension;
            $recipe->img_link = $image_name;
        }

        $user_id = Auth::id();
        $recipe->name = $request->input('name');
        $recipe->description = $request->input('description');
        $recipe->user_id = $user_id;
        $recipe->serves = $request->input('serves');
        $recipe->ingredients = $request->input('ingredients');
        

        
        $recipe->update(); //$recipe now has an id - already!!

        $recipe->instructions()->delete();

        for($i=1; $i<=10; $i++)
        {
            if($request->input('step' . $i))
            {
                $recipe->
                instructions()->
                create(['recipe_id' => $recipe->id,
                        'step_number' => $i,
                        'instruction' => $request->input('step' . $i)
                ]);
            }
        }
                
        return redirect(route('recipe.edit', $recipe->id));

    }

    function store(Request $request) //this function will store an entire row (recipe)
    {
        $this->validate($request, [ //WHAT IS IN $request: all the data in the text 
            'name' => 'required',  //example
            'serves' => 'required',
            'description' => 'required',
            'ingredients' => 'required',
            'step1' => 'required',
            'step2' => 'nullable',
            'step3' => 'nullable',
            'step4' => 'nullable',
            'step5' => 'nullable',
            'step6' => 'nullable',
            'step7' => 'nullable',
            'step8' => 'nullable',
            'step9' => 'nullable',
            'step10' => 'nullable',
            'image' => 'nullable'
            // USE THIS https://laravel.com/docs/5.8/validation
        ]);


        $file = $request->file('image');
        if($file)
        {
            $extension = $file->getClientOriginalExtension();
            Storage::disk('public')->put($file->getFilename().".".$extension, File::get($file));
            $image_name = $file->getFilename().".".$extension;
        }
        else {
            $image_name = 'noodles.jpg';
        }

        $user_id = Auth::id();
        $recipe = new Recipe();
        $recipe->name = $request->input('name');
        $recipe->description = $request->input('description');
        $recipe->user_id = $user_id;
        $recipe->serves = $request->input('serves');
        $recipe->ingredients = $request->input('ingredients');
        $recipe->img_link = $image_name;

        
        $recipe->save(); //$recipe now has an id - already!!


        for($i=1; $i<=10; $i++)
        {
            if($request->input('step' . $i))
            {
                $recipe->
                instructions()->
                create(['recipe_id' => $recipe->id,
                        'step_number' => $i,
                        'instruction' => $request->input('step' . $i)
                ]);
            }
        }
                
        return redirect("/recipes/upload");

    }
}