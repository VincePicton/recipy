<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auto-generated Auth routes:
Auth::routes();
////////////////////////////////////////////////////////



//mainpage routes:
Route::get('/', "RecipesController@index")->name('mainpage');

//the below does not follow convention
Route::get("/recipes/view/{recipe}", "RecipesController@show");

Route::get("/recipes/upload", "RecipesController@create");
////////////////////////////////////////////////////////


//Function which saves a new recipe to the database
Route::post("/recipes/store", "RecipesController@store")->name('recipe.store');
////////////////////////////////////////////////////////


//Function which deletes a recipe from the database
Route::delete('/recipes/{recipe}', 'RecipesController@destroy')->name('recipes.destroy');
////////////////////////////////////////////////////////


//Function which calls the edit page for a recipe
Route::get('/recipes/{id}/edit', 'RecipesController@edit')->name('recipe.edit');
////////////////////////////////////////////////////////


//Function which logs you out
Route::get('/recipy/logout', 'Auth\LoginController@logout')->name('recipy.logout');
////////////////////////////////////////////////////////


//Function which posts the update to a recipe after an edit by the user
Route::put('/recipes/{id}/update', 'RecipesController@update')->name('recipe.update');
////////////////////////////////////////////////////////


Route::get('/home', 'RecipesController@index')->name('home');
