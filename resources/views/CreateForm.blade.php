@extends('MasterPageStructure')
<?php use App\Categorie; ?>

@section('middlecolumn')
<div class="col white-background">
<br>
<h3>Create a Recipe</h3>
  <!-- need to route to a controller - look up redirects -->

<form action="{{route('recipe.store')}}" method="POST" enctype="multipart/form-data">
<!-- enctype thing makes images work, route is already made IT IS A DOT -->
  {{csrf_field()}} <!-- this line creates security -->
  <!-- @csrf() -->
  <br>
  <div class="form-row">
    <div class="col-10">
      <input name="name" class="form-control" type="text" placeholder="Recipe name">
    </div>
    <div class="col-2">
      <input name="serves" class="form-control" type="number" placeholder="Serves:">
    </div>
  </div>

  <div class="form-group">
    <label for="Recipe description"></label>
    <textarea name="description" class="form-control" id="Recipe description" rows="3" placeholder="Write a short description of your recipe. Be sure to include preparation and cooking time."></textarea>
  </div>

  <div class="form-group">
    <label for="Recipe ingredients"></label>
    <textarea name="ingredients" class="form-control" id="Recipe ingredients" rows="3" placeholder="Write what ingredients are needed. Be sure to include quantity and units for each item."></textarea>
  </div>

  <h5 class="pushtext-left">Now write your step-by-step guide</h5>
  @for($i=1; $i<=10; $i++)
  <input name="step<?= $i ?>" class="form-control form-control-sm sm-margin-top" type="text" placeholder="Step <?= $i ?>">
  @endfor

  <br>
  <h5 class="pushtext-left">Label your recipe with up 5 categories</h5>
  <div class="form-row align-items-center">
<?php
  $categories = Categorie::all();
?>
  @for($i=1; $i <=5; $i++)
    <?php $count = 1; ?>
    <div class="col">
      <label class="mr-sm-2 sr-only" for="Cat<?= $i ?>">Preference</label>
      <select name="cat<?= $i ?>" class="custom-select mr-sm-2" id="Cat<?= $i ?>">
        <option selected>Choose...</option>
        
        @foreach($categories as $categorie)
          <option value="$count"><?= $categorie->name ?></option>
          <?php $count++ ?>
        @endforeach

      </select>
    </div>
  @endfor

  </div>
  <br>
  
<!--   <input class="form-control" type="text" placeholder="Default input"> -->

  <br><h5 class="pushtext-left">Attach an image of your finished meal (optional)</h5>
  <input type="file" class="form-control-file" name="image"> <!-- this gets an image - name must match the name in the controller for each input -->
  <!-- the class for images is "form-control-file" type="file" -->

  <input type="submit" value="Submit">
</form>
</div>
@endsection
