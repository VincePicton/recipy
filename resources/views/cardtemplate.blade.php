<?php use App\Recipe; ?>

<head>

    <?php
        $user_id = 1; //can be changed in future
        $recipe_id = $recipe->id; //here we should initially load the most recent posts by the user, later load most recent post in the set of posts by the user's followees.
        //$imgLink = (Recipe::find($recipe_id))['img_link'];
        $name = (Recipe::find($recipe_id))['name'];
        $description = (Recipe::find($recipe_id))['description'];
        $serves = (Recipe::find($recipe_id))['serves'];
        $showLink = "recipes/view/" . $recipe_id ;
    ?>

</head>

<body>

    <div class="card flex-row flex-wrap">

    <div class="row">
        <div class="col-md-4">
            <img src="/images/<?= $recipe->img_link ?>" alt="Finished meal" class="card-image border border-dark">
        </div>

        <div class="col-md-8">

            <div class="row-2 row">
                <div class="col-12"> <!-- col-12 this is small, col-md-12 this is medium screen size, col-lg-12 this is large screen size find out using tis -->
                    <div class="h4">{{$name}}   |   Serves: {{$serves}}</div>
                </div>
            </div>
            <div class="row-7 row sm-margin-top">
                <div class="col-12">
                    <p class="">{{$description}}</p>
                </div>
            </div>
            <div class="row-3 row pos-absolute">
                <div class="col-12">
                    <a href={{$showLink}} class="btn btn-primary">View</a>
                </div>
            </div>
            
        </div>
        </div>
    </div>

</body>