@extends('MasterPageStructure')

    <title>Recipy</title>

    <!-- get all the IDs for the user as an array, then cycle through them with an foreach loop,
    pasting the entire div with variable of recipe_id changing in each -->

    <!-- https://www.youtube.com/watch?v=emyIlJPxZr4 -->



<!--   END OF HEAD ----------------------------------------------------------------------------- -->

<!--   START OF BODY --------------------------------------------------------------------------- -->

<!--       @foreach ($usersRecipes as $recipe) this is how to get individual items of data!
      
        {{$recipe->id}}
        {{$recipe->user_id}}
        {{$recipe->img_link}}
      
      @endforeach -->
    

@section('middlecolumn')
  <div class="text-center">
    {{ $usersRecipes->links() }}
  </div>

  @foreach($usersRecipes as $recipe)
    <div>

    @include('cardtemplate')

    </div>
  @endforeach

@endsection
