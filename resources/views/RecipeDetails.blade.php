<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags for bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/VinceBootstrapEdit.css" crossorigin="anonymous"> <!-- CAN ADD TO END OF FILE ?v=0.1-->

    <title>Recipe details</title> <!-- CHANGE TO ACTUAL RECIPE NAME W PHP -->

  </head>

  <body class="recipy-background">

    @include('navbar')

  <div class="container text-center mt-0 relative">  <!-- border rounded-lg -->

    <div class="col">

      <div class="top-details-card">
        
        <div class="col-3 card-body">
          <img src="/images/{{ $recipe->img_link }}" alt="Finished meal" class="card-image border border-dark">
          <div>
            <h5>Posted by {{$recipe->user->name}}</h5>
          </div>
        </div>

        <div class="col-9 card-body">
          <div class="row-2 card-title">
              <h3>{{$recipe->name}} | Serves: {{$recipe->serves}}</h3>
          </div>
          <div class="row-10 align-self-center">
            <p class="align-self-center">{{$recipe->description}}</p>
          </div>
        </div>

      </div>
      <!-- /.card -->

      <div class="top-details-card card-outline-secondary my-4">
        <div class="card-header">
          Ingredients
        </div>
        <div class="card-body pushtext-left">
          <p>{{$recipe->ingredients}}</p>
        </div>
      </div>
      <!-- /.card -->

      <div class="card card-outline-secondary">
        <div class="card-header">
          Instructions
        </div>

          <?php $instructions = $recipe->instructions; ?> <!-- NO BRACKETS!!! -->
          <div class="card-body pushtext-left">

          @foreach($instructions as $instruction)

              <p><b>{{$instruction['step_number']}}.</b> {{$instruction['instruction']}}</p><hr>

          @endforeach

          </div>
      </div>
      <!-- /.card -->

      
      @if ($recipe->user_id == Auth::id()) <!-- HERE IS HOW TO GET CURRENT USER ID -->
      <div class="row jc-center sm-margin-top">
        <form action="{{route('recipes.destroy', $recipe->id)}}" method="POST">
          <button class="btn btn-danger">Delete</button>
          @method('DELETE') <!-- PUT is also possible here - HTML forms only allow POST and GET in their method, so you have to do this hidden method thing -->
          @csrf
        </form>
        <a href="{{route('recipe.edit', $recipe->id)}}" class="btn btn-primary">Edit</a>
      </div>
      
      @endif

    </div>

  </div> <!-- container ends here -->





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>