<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('categories')->insert([
	    	'name' => 'Vegan'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Vegetarian'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Thai'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Chinese'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Tex Mex'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Italian'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Spicy'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Greek'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Hearty'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Cold'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Salad'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Dessert'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Starter'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Main'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Beef'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Pork'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Chicken'
	    ]);
	    DB::table('categories')->insert([
	    	'name' => 'Fish'
	    ]);
    }
}
